===============================
Genophore Test
===============================


Installation and run from repo
----------------------------

#. Clone the `git repository <https://gitlab.com/waseemkn/genophoretest.git>`_.

#. Install all third party packages by running::

    $ pip install -r requirements.txt


#. Apply migrations::

    $ python manage.py migrate

#. Run the server::

    $ python manage.py runserver


XSS Attack:
-----------
You can test the XSS attack issue by access this url "/genapp/view-test".

Test Various Input:
----------------
You can see the input test cases in genapp/tests.py and you can run them by::

    $ python manage.py test


Frontend Task:
-------------
You can find it in genephore_test/frontend_task/


========
`Have Fun :)`
========

`Waseem Kntar, Software Engineer`
