class GenControl extends BaseControl {
    constructor(container) {
        super(container);

        this.IDs = {
            headerElement: 'header-element',
            labelInputElement: 'label-input',
            inputElement: 'input-element',
            labelSelectElement: 'label-select',
            selectElement: 'select-element',
            buttonElement: 'button-element',
            textLabelElement: 'text-label-element'
        };

        this.events = {
            headerClicked: 'header-clicked',
            addValueClicked: 'addvalue-clicked'
        };
    }
    _initUI() {
        super._initUI();

        this.mainElement.append(`
            <h1 class="${this.absoluteID(this.IDs.headerElement)}"></h1>
        `);
        // input field
        this.mainElement.append(`
            <label id=${this.absoluteID(this.IDs.labelInputElement)}>Value: </label>
        `)
        this.mainElement.append(`
            <input id=${this.absoluteID(this.IDs.inputElement)} type="text" class="${this.absoluteID(this.IDs.inputElement)}"></input>
        `);
        
        this.mainElement.append(`
            <br>
        `)

        // button
        this.mainElement.append(`
            <button id=${this.absoluteID(this.IDs.buttonElement)} class=${this.absoluteID(this.IDs.buttonElement)}>Add value</button>
        `)
        
        
        this.mainElement.append(`
            <br>
        `)

        // dropdown menu
        this.mainElement.append(`
            <label id=${this.absoluteID(this.IDs.labelSelectElement)}>Values Set: </label>
        `)
        this.mainElement.append(`
            <select id=${this.absoluteID(this.IDs.selectElement)}>
                <option value="volvo">Volvo</option>
                <option value="saab">Saab</option>
                <option value="mercedes">Mercedes</option>
                <option value="audi">Audi</option>
            </select>
        `)
        
        this.mainElement.append(`
            <br>
        `)

        // text label
        this.mainElement.append(`
            <label id=${this.absoluteID(this.IDs.textLabelElement)}>Text Label</label>
        `)

        this.defineEvents()

    }
    
    defineEvents() {

        this.elementOfHTMLID(this.IDs.buttonElement).on('click',  () => {
            let inputElement = this.elementOfHTMLID(this.IDs.inputElement)[0];
            let textLabelElement = this.elementOfHTMLID(this.IDs.textLabelElement);
            let selectElement = this.elementOfHTMLID(this.IDs.selectElement);
            let opticurrentSelectOptionsTexts = [...selectElement[0].options].map(o => o.text)
            if (inputElement.value) {
                if (!opticurrentSelectOptionsTexts.includes(inputElement.value)) {
                    selectElement[0].innerHTML += `<option value="${inputElement.value}">${inputElement.value}</option>`
                    textLabelElement[0].innerHTML = `This value "${inputElement.value}" is added to the dropdown menu.`
                }
                else {
                    textLabelElement[0].innerHTML = `This value "${inputElement.value}" is already existed`
                }
            }
                
            else {
                
                textLabelElement[0].innerHTML = 'The input field is empty!'
            }
                
            this.trigger(this.events.addValueClicked);

        });
    }
    


}
