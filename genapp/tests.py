from django.core.exceptions import ValidationError
from django.test import TestCase

# Create your tests here.
from genapp.models import Protein, Document, Comment, Notification


class TestVariousInput(TestCase):

    def test_protein_false_input(self):
        protein = Protein(aa_sequence='123', dna_sequence='1234')
        with self.assertRaises(ValidationError):
            protein.full_clean()

    def test_protein_true_input(self):
        protein = Protein(aa_sequence='123', dna_sequence='123456789') # here dna_sequence length is right
        self.assertIsNone(protein.full_clean())

    def test_creating_document(self):
        doc = Document.objects.create(text='Document Text', name='Document Name')
        print('\n\n------------ Test Creating A Document ------------')
        print(f'name: {doc.name}\ntext: {doc.text}\ncreated: {doc.created}\nmodified: {doc.modified}')
        print('------------ ------------------------ ------------')

    def test_creating_document_comment(self):
        document = Document.objects.create(text='Document Text', name='Document Related With Comment')
        document_comment = Comment.objects.create(text='A document comment', content_object=document)
        print('\n\n------------ Test Creating A Document Comment (By a Generic Relation) ------------')
        print(f'comment: {document_comment.text}\ndocument_object Name that in comment: {document_comment.content_object.name}')
        print('------------ -------------------------------------------------------- ------------')

    def test_creating_protein_comment(self):
        protein = Protein.objects.create(aa_sequence='123', dna_sequence='123456789')
        document_comment = Comment.objects.create(text='A document comment', content_object=protein)
        print('\n\n------------ Test Creating A Protein Comment (By a Generic Relation) ------------')
        print(f'comment: {document_comment.text}\nprotein_object AA Seq that in comment: {document_comment.content_object.aa_sequence}')
        print('------------ ----------------------------------------------------------- ------------')

    def test_creating_protein_notification(self):
        protein = Protein.objects.create(aa_sequence='123', dna_sequence='123456789')
        notification = Notification.objects.create(type='Link', message='google.com', icon='any', content_object=protein)
        print('\n\n------------ Test Creating A Protein Notification (By a Generic Relation) ------------')
        print(f'notification message: {notification.message}\nprotein_object AA Seq that in notification: {notification.content_object.aa_sequence}')
        print('------------ ---------------------------------------------------------------- ------------')
