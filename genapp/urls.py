
from django.urls import path, include

from genapp.views import ViewTest

urlpatterns = [
    path("view-test/", ViewTest.as_view(), name='view_test')
]
