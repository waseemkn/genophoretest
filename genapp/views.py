from django.shortcuts import render

# Create your views here.
from django.views import View

from lxml.html.clean import clean_html


class ViewTest(View):
    def get(self, request, *args, **kwargs):
        # without cleaning the evil alert will be executed
        context = {
            'html_string': clean_html('<h1>Text</h1><script type="text/javascript">alert("evil");</script>')
        }
        return render(request, 'view_test.html', context)
