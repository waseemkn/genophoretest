from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.
from lxml.html.clean import clean_html


class Document(models.Model):
    name = models.CharField(max_length=20)
    text = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def clean_text(self):
        return clean_html(self.text)

    def __str__(self):
        return self.name


class Protein(models.Model):
    aa_sequence = models.TextField()
    dna_sequence = models.TextField()

    class Meta:
        unique_together = ('aa_sequence', 'dna_sequence',)

    def full_clean(self, exclude=None, validate_unique=True, validate_constraints=True):
        super().full_clean(exclude, validate_unique, validate_constraints)
        if self.aa_sequence and self.dna_sequence and len(self.dna_sequence) != 3*len(self.aa_sequence):
            raise ValidationError(_('There is an error in AA Sequence or DNA Sequence, the lengths are not standard.'))


class Comment(models.Model):
    text = models.TextField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def clean_text(self):
        return clean_html(self.text)


class Notification(models.Model):
    TYPES = [
        ('type_1', 'Link'),
        ('type_2', 'Static'),
        ('type_3', 'System'),
    ]
    type = models.CharField(max_length=20, choices=TYPES)
    message = models.TextField()
    icon = models.CharField(max_length=200)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created = models.DateTimeField(auto_now_add=True)
